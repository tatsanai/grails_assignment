package mystore

import grails.plugin.springsecurity.annotation.Secured

class HomeController {

    def productService
    static def allowedMethods = [store: "POST"]

    def index() {
        def products = Product.findAllByIsPublishedAndSaleDateLessThan(true, new Date())
        render(view: "index", model: [products: products])
    }

    @Secured(["ROLE_USER", "ROLE_ADMIN"])
    def create() {
        render(view: "create", model: [product: new Product()])
    }

    @Secured(["ROLE_USER", "ROLE_ADMIN"])
    def store(Product product) {
        withForm {
            def error = productService.saveProduct(product)
            if (error == "")
                redirect(action: "show", params: [id: product.id])
            else
                render(view: "create", model: [error: error])
        }.invalidToken {
            render(view: "index", model: [error: "ERROR : Invalid Token !"])
        }
    }

    def show(Product product) {
//        CHECK IF CURRENT USER == PRODUCT.ADDED_BY
//            SHOW PRODUCT DETAIL EVEN IF IT'S NOT PUBLISHED
        if(product.isPublished)
            render(view: "show", model: [product: product])
        else
            render(view: "invalid")
    }

    @Secured(["ROLE_USER", "ROLE_ADMIN"])
    def edit(Product product) {
//        CHECK IF CURRENT USER == PRODUCT.ADDED_BY
        render(view: "edit", model: [product: product])
    }

    @Secured(["ROLE_USER", "ROLE_ADMIN"])
    def update(Product product) {
        withForm {
            def error = productService.saveProduct(product)
            if (error == "")
                redirect(action: "show", params: [id: product.id])
            else
                render(view: "edit", model: [product: product, error: error])
        }.invalidToken {
            render(view: "index", model: [error: "ERROR : Invalid Token !"])
        }
    }

    def init() {
        def user = User.findAllByUsername("admin")
        def role = Role.findAllByAuthority("ROLE_ADMIN")
        def role_u = Role.findAllByAuthority("ROLE_USER")
        User user_admin = new User(username: "admin", password: "admin", enabled: true)
        Role role_admin = new Role(authority: "ROLE_ADMIN")
        Role role_user = new Role(authority: "ROLE_USER")
        if(user.size() == 0)
            user_admin.save(flush: true)
        if(role.size() == 0) {
            role_admin.save(flush: true)
            UserRole.create(user_admin, role_admin)
        }
        if(role_u.size() == 0)
            role_user.save(flush: true)
        redirect(action: "index")
    }

}
