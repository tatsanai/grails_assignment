package mystore

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class AdminRoleController {

    def adminRoleService
    static def allowedMethods = [store: "POST"]

    def index() {
        render(view: "index", model: [roles: Role.findAll()])
    }

    def create() {
        render(view: "create", model: [role: new Role()])
}

    def store(Role role) {
        withForm {
            def error = adminRoleService.saveRole(role)
            if (error == "")
                redirect(action: "index")
            else
                render(view: "create", model: [error: error])
        }.invalidToken {
            render(view: "index", model: [error: "ERROR : Invalid Token !"])
        }
    }
}
