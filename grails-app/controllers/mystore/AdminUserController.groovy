package mystore

import grails.plugin.springsecurity.annotation.Secured
import mystore.User

@Secured(['ROLE_ADMIN'])
class AdminUserController {

    def adminUserService
    static def allowedMethods = [store: "POST", update: "POST", delete: ["POST", "DELETE"]]

    def index() {
        render(view: "index", model: [users: User.findAll()])
    }

    def create() {
        render(view: "create", model: [user: new User()])
    }

    def store(User user) {
        withForm {
            if(params.password == params.confirmed_password) {
                def status = adminUserService.saveUser(user)
                if (status == "")
                    redirect(action: "index")
                else
                    render(view: "create", model: [error: status])
            } else {
                render(view: "create", model: [error: "ERROR : password != confirmed_password"])
            }
        }.invalidToken {
            render(view: "index", model: [error: "ERROR : Invalid Token !"])
        }
    }

    def show(User user) {
        render(view: "show", model: [user: user])
    }

    def edit(User user) {
        render(view: "edit", model: [user: user])
    }

    def update(User user) {
        withForm {
            if(params.password == params.confirmed_password) {
                def status = adminUserService.saveUser(user)
                if(status == "")
                    redirect(action: "index")
                else
                    render(view: "edit", model: [user: user, error: status])
            } else {
                render(view: "edit", model: [user: user, error: "ERROR : password != confirmed_password"])
            }
        }.invalidToken {
            render(view: "index", model: [error: "ERROR : Invalid Token !"])
        }
    }

    def delete(User user) {

    }

}
