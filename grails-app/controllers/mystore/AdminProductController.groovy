package mystore

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class AdminProductController {

    def productService
    static def allowedMethods = [store: "POST", update: "POST"]

    def index() {
        render(view: "index", model: [products: Product.findAll()])
    }

    def create() {
        render(view: "create", model: [product: new Product()])
    }

    def store(Product product) {
        withForm {
            def error = productService.saveProduct(product)
            if(error == "")
                redirect(action: "index")
            else
                render(view: "create", model: [error: error])
        }.invalidToken {
            render(view: "index", model: [error: "ERROR : Invalid Token !"])
        }
    }

    def show(Product product) {
        render(view: "show", model: [product: product])
    }

    def edit(Product product) {
        render(view: "edit", model: [product: product])
    }

    def update(Product product) {
        withForm {
            def error = productService.saveProduct(product)
            if(error == "")
                redirect(action: "index")
            else
                render(view: "edit", model: [product: product, error: error])
        }.invalidToken {
            render(view: "index", model: [error: "ERROR : Invalid Token !"])
        }
    }

    def delete(Product product) {
        if(product == null)
            render(action: "index")
        def status = productService.deleteProduct(product)
        if(request.xhr) {
            render([status: status, id: params.id] as JSON)
        } else {
            redirect(action: "index")
        }
    }
}
