<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>CMS : Role List</title>
    </head>

    <body>
        <h1>CMS : Role List</h1>
        <g:link action="create">[+] Add Role</g:link>
        <table>
            <th>
            <td>Role</td>
        </th>
            <g:each in="${roles}" var="role" status="i">
                <tr>
                    <td>${i+1}</td>
                    <td>${role.authority}</td>
                </tr>
            </g:each>
        </table>
    </body>
</html>