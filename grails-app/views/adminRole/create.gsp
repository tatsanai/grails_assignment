<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>CMS : Create Role</title>
    </head>

    <body>
        <h1>Create Role</h1>
        <p>${error}</p>
        <g:form action="store" useToken="true">
            <label name="authority">Role:</label>
            <g:textField name="authority"></g:textField>
            <br>
            <g:submitButton name="submit" value="Add Role"></g:submitButton>
        </g:form>
    </body>
</html>