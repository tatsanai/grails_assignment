<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>${product?.name} | Simple Online Store</title>
    </head>

    <body>
        <h1>${product?.name}</h1>
        <h3>THB ${product?.salePrice}</h3>
    %{-- IF current_user == added_by --}%
        <hr>
    %{-- IF current_user == added_by --}%
    %{--    Display "Added by you" --}%
        <p><i>Sales Date: ${product?.saleDate} // Added By: ${product?.addedBy}</i></p>
        <br>
        <p>${product?.description}</p>
        <br>
        <g:link action="index" controller="home">< Back</g:link>
    </body>
</html>