<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>Simple Online Store</title>
    </head>

    <body>
    %{-- IF not login --}%
        <sec:ifNotLoggedIn>
    %{--    Login Form --}%
            <g:link controller='login' action='auth'>Login</g:link><br>
        </sec:ifNotLoggedIn>
        <sec:ifLoggedIn>
            <h3>Welcome, <sec:loggedInUserInfo field="username"/></h3>
            <g:link controller='logoff'>Logout</g:link><br>
        </sec:ifLoggedIn>
        <hr>
    %{-- IF Login as admin --}%
        <sec:ifAllGranted roles="ROLE_ADMIN">
    %{--    User CMS Link || Product CMS Link --}%
            <g:link controller='adminUser' action='index'>User CMS</g:link><br>
            <g:link controller='adminRole' action='index'>Role CMS</g:link><br>
            <g:link controller='adminProduct' action='index'>Product CMS</g:link>
            <hr>
        </sec:ifAllGranted>
        <h1>Browse Products</h1>
        <g:link action="create">[+] Create</g:link>
        <p>${error}</p>

        <g:each in="${products}" var="product" status="i">
            <h3>${i+1}.) <g:link action="show" params="[id: product.id]">${product.name}</g:link></h3>
        %{-- IF current_user == added_by --}%
        %{--    Display "Added by you" --}%
            <p><i>Sale Date : ${product.saleDate} // Added By ${product.addedBy}</i></p>
            <h3>THB ${product.salePrice}</h3>
            <br><hr>
        </g:each>
    </body>
</html>