<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>Edit ${product.name} | Simple Online Store</title>
    </head>

    <body>
        <h1>Edit : ${product.name}</h1>
        <p>${error}</p>
        <g:form action="store" useToken="true">
            <g:hiddenField name="id" value="${product.id}"></g:hiddenField>
            <label name="name">Product Name:</label>
            <g:textField name="name" value="${product.name}"></g:textField>
            <br>
            <label name="description">Description:</label>
            <g:textArea name="description" value="${product.description}"></g:textArea>
            <br>
            <label name="salePrice">Sales Price:</label>
            <g:textField name="salePrice" value="${product.salePrice}"></g:textField>
            <br>
            <label name="saleDate">Sales Date:</label>
            <g:datePicker name="saleDate" precision="day" value="${product.saleDate}"></g:datePicker>
            <br>
            <label name="isPublished">Published</label>
            <g:checkBox name="isPublished" value="${product.isPublished}"></g:checkBox>
            <br>
            <g:submitButton name="submit" value="Edit Product"></g:submitButton>
        </g:form>
    </body>
</html>