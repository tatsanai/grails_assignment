<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>Product not found | Simple Online Store</title>
    </head>

    <body>
        <h1>This product is invalid or not available at this time.</h1>
        <g:link action="index" controller="home">< Back</g:link>
    </body>
</html>