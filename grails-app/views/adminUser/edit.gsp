<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>CMS : Edit User</title>
    </head>

    <body>
    <h1>Edit User ${user.username}</h1>
    <p>${error}</p>
    <g:form action="update" useToken="true">
        <g:hiddenField name="id" value="${user.id}"></g:hiddenField>
        <label name="username">Username:</label>
        <g:textField name="username" value="${user.username}"></g:textField>
        <br>
        <label name="password">Password:</label>
        <g:passwordField name="password"></g:passwordField>
        <br>
        <label name="confirmed_password">Re-enter Password</label>
        <g:passwordField name="confirmed_password"></g:passwordField>
        <br>
        <g:submitButton name="submit" value="Add User"></g:submitButton>
    </g:form>
    </body>
</html>