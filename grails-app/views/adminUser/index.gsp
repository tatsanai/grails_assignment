<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>CMS : User List</title>
    </head>

    <body>
        <h1>CMS : User List</h1>
        <g:link action="create">[+] Add User</g:link>
        <g:link action="index" controller="adminRole">Manage Role ></g:link>
        <table>
            <th>
            <td>Username</td>
            <td>User Detail</td>
        </th>
            <g:each in="${users}" var="user" status="i">
                <tr>
                    <td>${i+1}</td>
                    <td>${user.username}</td>
                    <td>
                        <g:link action="show" params="[id: user.id]">Show ></g:link>
                        <g:link action="edit" params="[id: user.id]">Edit ></g:link>
                    </td>
                </tr>
            </g:each>
        </table>
    </body>
</html>