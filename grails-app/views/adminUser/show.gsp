<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>CMS : ${user?.username}</title>
    </head>

    <body>
        <h1>User Detail</h1>
        <hr>
        <g:link action="edit">[-] Edit</g:link>
        <p><b>Username:</b> ${user?.username}</p>
        <p><b>Password:</b> ${user?.password}</p>
        <g:link action="index">< Back</g:link>
    </body>
</html>