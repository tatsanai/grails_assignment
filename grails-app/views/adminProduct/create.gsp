<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>CMS : Create Product</title>
    </head>

    <body>
        <h1>Create Product</h1>
        <p>${error}</p>
        <g:form action="store" useToken="true">
            <label name="name">Product Name:</label>
            <g:textField name="name"></g:textField>
            <br>
            <label name="description">Description:</label>
            <g:textArea name="description"></g:textArea>
            <br>
            <label name="salePrice">Sales Price:</label>
            <g:textField name="salePrice"></g:textField>
            <br>
            <label name="saleDate">Sales Date:</label>
            <g:datePicker name="saleDate" precision="day"></g:datePicker>
            <br>
            <label name="isPublished">Published</label>
            <g:checkBox name="isPublished"></g:checkBox>
            <br>
            <g:submitButton name="submit" value="Add Product"></g:submitButton>
        </g:form>
    </body>
</html>