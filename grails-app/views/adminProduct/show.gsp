<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>CMS : Product Detail > ${product?.name}</title>
    </head>

    <body>
        <h1>CMS : Show Product Detail > ${product?.name}</h1>
        <hr>
        <p><b>Description:</b> ${product?.addedBy}</p>
        <p><b>Description:</b> ${product?.description}</p>
        <p><b>Sales Price:</b> ${product?.salePrice}</p>
        <p><b>Sales Date:</b> ${product?.saleDate}</p>
        <p><b>Published:</b> ${product?.isPublished}</p>
        <g:link action="index">< Back</g:link>
    </body>
</html>