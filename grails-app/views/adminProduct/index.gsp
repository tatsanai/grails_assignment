<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>CMS : Product List</title>
    </head>

    <body>
        <h1>CMS : Product List</h1>
        <g:link action="create">[+] Create</g:link>
        <table>
            <th>
            <td>Name</td>
            <td>Added By</td>
            <td>Sale Price</td>
            <td>Published</td>
            <td>Product Detail</td>
        </th>
            <g:each in="${products}" var="product" status="i">
                <tr>
                    <td>${i+1}</td>
                    <td>${product.name}</td>
                    <td>${product.addedBy}</td>
                    <td>${product.salePrice}</td>
                    <td>${product.isPublished}</td>
                    <td>
                        <g:link action="show" params="[id: product.id]">Show ></g:link><t>
                        <g:link action="edit" params="[id: product.id]">Edit ></g:link>
                        <g:link action="delete" params="[id: product.id]">Delete ></g:link>
                    </td>
                </tr>
            </g:each>
        </table>
    </body>
</html>