<!DOCTYPE HTML>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>CMS : Edit Product > ${product.name}</title>
    </head>

    <body>
        <h1>CMS : Edit Product > ${product.name}</h1>
        <p>${error}</p>
        <g:form action="update" useToken="true">
            <g:hiddenField name="id" value="${product.id}" />
            <label name="name">Product Name:</label>
            <g:textField name="name" value="${product.name}"></g:textField>
            <br>
            <label name="description">Description:</label>
            <g:textArea name="description" value="${product.description}"></g:textArea>
            <br>
            <label name="salePrice">Sales Price:</label>
            <g:textField name="salePrice" value="${product.salePrice}"></g:textField>
            <br>
            <label name="saleDate">Sales Date:</label>
            <g:datePicker name="saleDate" value="${product.saleDate}" precision="day"></g:datePicker>
            <br>
            <label name="isPublished">Published</label>
            <g:checkBox name="isPublished" value="${product.isPublished}"></g:checkBox>
            <br>
            <g:submitButton name="submit" value="Edit Product"></g:submitButton>
        </g:form>
    </body>
</html>