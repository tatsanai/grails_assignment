package mystore

import grails.transaction.Transactional

@Transactional
class ProductService {

    def springSecurityService

    def serviceMethod() {

    }

    def saveProduct(Product product) {
        def status = ""
        product.addedBy = springSecurityService.getCurrentUser().username
        if(product.validate())
            product.save()
        else
            status = "ERROR : product.validate = FALSE"
        return status
    }

    boolean deleteProduct(Product product) {
        product.delete(flush: true)
        return (product != null)
    }

}
