package mystore

import grails.transaction.Transactional

@Transactional
class AdminRoleService {

    def serviceMethod() {

    }

    def saveRole(Role role) {
        def status = ""
        if(role.validate()) {
            role.authority = "ROLE_" + (role.authority).toUpperCase()
            role.save()
        } else
            status = "ERROR : role.validate = FALSE"
        return status
    }

}
