package mystore

import grails.transaction.Transactional

@Transactional
class AdminUserService {

    def serviceMethod() {

    }

    def saveUser(User user) {
        def status = ""
        user.enabled = true
        if(user.validate())
            user.save(flush: true)
        else
            status = "ERROR : user.validate = FALSE"
        UserRole.create(user, Role.findByAuthority("ROLE_USER"))
        return status
    }

}
