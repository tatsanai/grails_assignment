package mystore

class Product {

//  states

    String name
    String addedBy
    String description
    Date saleDate
    double salePrice
    boolean isPublished

    static constraints = {
        name blank: false, size: 1..256
        addedBy blank: false
        description blank: false
        saleDate nullable: true
        salePrice blank: false
    }

    static mapping = {
        table 'products'
        version false
    }

    Product(String name, String description, Date saleDate, double salePrice, boolean isPublished) {
        this.name = name
        this.description = description
        this.saleDate = saleDate
        this.salePrice = salePrice
        this.isPublished = isPublished
    }

}
